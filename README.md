Demo-react
---

Forked from: https://github.com/ahfarmer/calculator

## Docker set-up

Download the pre-built image and start the service with this command:

    docker run --rm -it -p 8080:80 registry.gitlab.com/simevo/demo-react/demo-react

then visit the app at: http://localhost:8080.

Alternatively, build the docker image locally with:

    docker build . -t demo-react

and run the locally built image with:

    docker run --rm -it -p 8080:80 demo-react

## Kubernetes set-up

Install [minikube](https://minikube.sigs.k8s.io/docs/start/) and [kubectl](https://kubernetes.io/docs/tasks/tools/install-kubectl-linux/#install-using-native-package-management).

Download this repo:

    git clone git@gitlab.com:simevo/demo-react.git
    cd demo-react

Deploy to minikube:

    kubectl apply -f deployment.yaml

[Export the service from minikube](https://minikube.sigs.k8s.io/docs/commands/service/):

    minikube service --url demo-react

this will return something like `http://192.168.39.204:32080`. You can directly access that url on the host where minikube is running; if you are on a different host you must forward the port with:

    ssh -L 8080:192.168.39.204:31360 example.com
    
then you will be able to access the service at http://localhost:8080.

To deploy a separate instance on anoher pod, duplicate `deployment.yaml`, rename service + deployment and deploy the new application:

    cp deployment.yaml deployment1.yaml
    sed -i 's/name: demo-react/name: demo-react1/g' deployment1.yaml

When you're done, tear down the deployments:

    kubectl delete -f deployment.yaml
    kubectl delete -f deployment1.yaml

---

Original readme:

Calculator
---
<img src="Logotype primary.png" width="60%" height="60%" />

Created with *create-react-app*. See the [full create-react-app guide](https://github.com/facebookincubator/create-react-app/blob/master/packages/react-scripts/template/README.md).



Try It
---

[ahfarmer.github.io/calculator](https://ahfarmer.github.io/calculator/)



Install
---

`npm install`



Usage
---

`npm start`
