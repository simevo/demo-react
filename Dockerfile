FROM node:16 AS deps
WORKDIR /app
COPY ./package.json ./yarn.lock ./
RUN yarn

FROM deps AS builder
WORKDIR /app
COPY public ./public
COPY src ./src
RUN yarn build

FROM nginx AS frontend
COPY --from=builder /app/build /usr/share/nginx/html
